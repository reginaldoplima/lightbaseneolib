﻿using System.Threading.Tasks;
using LightBaseNeoLib.Interfaces;
using System.Net.Http;
using System;
using System.Net.Http.Headers;
using System.Collections.Generic;

namespace LightBaseNeoLib.Core
{
    public class LightBaseNeoClient : ILightbaseNeoClient
    {
        private HttpClient _client;
        private readonly Uri _root;

        public LightBaseNeoClient(ILightBaseNeoConfig _config)
        {
            _root = _config.LightBaseUri;
            _client = new HttpClient();
        }

        public Task<HttpResponseMessage> Delete(string path)
        {
            string uri = _root.AbsoluteUri + path;
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Delete, uri);
            return _client.SendAsync(request);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            _client.Dispose();
        }

        public Task<HttpResponseMessage> Get(string path)
        {
            string uri = _root.AbsoluteUri + path;
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, uri);
            return _client.SendAsync(request);
        }

        public Task<HttpResponseMessage> Post(string path, List<KeyValuePair<string, string>> data)
        {
            string uri = _root.AbsoluteUri + path;
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, uri);
            request.Headers.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
            request.Content = new FormUrlEncodedContent(data);
            return _client.SendAsync(request);
        }

        public Task<HttpResponseMessage> PostRawJson(string path, string rawData)
        {
            string uri = _root.AbsoluteUri + path;
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, uri);
            request.Headers.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            request.Content = 
                new StringContent(rawData, System.Text.Encoding.UTF8, "application/json");
            return _client.SendAsync(request);
        }

        public Task<HttpResponseMessage> Put(string path, HttpContent httpContent)
        {
            string uri = _root.AbsoluteUri + path;
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Put, uri)
            {
                Content = httpContent
            };
            return _client.SendAsync(request);
        }

        public async Task<HttpResponseMessage> Patch(string path, List<KeyValuePair<string, string>> data)
        {
            string uri = _root.AbsoluteUri + path;
            HttpRequestMessage request = new HttpRequestMessage(new HttpMethod("PATCH"), uri);
            request.Headers.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
            request.Content = new FormUrlEncodedContent(data);
            var response = await _client.SendAsync(request);
            return response;
        }

        public Task<HttpResponseMessage> PostFile(string path, StreamContent streamContent, string name, string fileName)
        {
            string uri = _root.AbsoluteUri + path;
            var requestContent = new MultipartFormDataContent
            {
                { streamContent, name, fileName }
            };
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, uri)
            {
                Content = requestContent
            };
            return _client.SendAsync(request);
        }
    }
}