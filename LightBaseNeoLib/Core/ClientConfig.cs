﻿using LightBaseNeoLib.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightBaseNeoLib.Core
{
    public class ClientConfig : ILightBaseNeoConfig
    {
        public ClientConfig(Uri uri)
        {
            LightBaseUri = uri;
        }
        public Uri LightBaseUri { get; set; }
    }
}
