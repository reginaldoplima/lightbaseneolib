﻿using LightBaseNeoLib.Interfaces;
using System;

namespace LightBaseNeoLib.Core
{

    public class NeoBase : BaseContent
    {

        public NeoBase()
        {
            Metadata = new BaseMetadata
            {
                Color = "#000",
                Name = "",
                Password = "aaaa",
                Description = "description",
                Admin_Users = new string[] {""},
                File_Ext = false,
                File_ext_time = 0,
                Idx_exp = false,
                Idx_Exp_Time = 0,
                Idx_exp_url = "",
                Owner = "",
            };
        }
    }
}
