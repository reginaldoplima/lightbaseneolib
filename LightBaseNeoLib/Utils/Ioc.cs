﻿using LightBaseNeoLib.Core;
using LightBaseNeoLib.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightBaseNeoLib.Utils
{
    public class Ioc
    {
        private static Uri _lbneoUri = new Uri("http://localhost:6543");
        public static void Register(IServiceCollection services)
        {
            var config = new ClientConfig(_lbneoUri);
            var serializer = new JsonSerializer();
            services.AddSingleton<ILightBaseNeoConfig>(config);
            services.AddSingleton<ISerializer>(serializer);
            services.AddScoped<ILightbaseNeoClient, LightBaseNeoClient>();
            services.AddScoped<ILightBaseNeoApp, LightBaseNeoApp>();
        }
    }
}
