﻿using LightBaseNeoLib.Core;
using LightBaseNeoLib.Interfaces;
using System.Collections.Generic;
using System.Reflection;

namespace LightBaseNeoLib.Utils
{
    public class LbNeoMap
    {

        private static List<KeyValuePair<string, string>> mapNeoPrimitives = new List<KeyValuePair<string, string>>
        {

            new KeyValuePair<string, string>("Int32","number"),
            new KeyValuePair<string, string>("bool","bolean"),
            new KeyValuePair<string, string>("String","Text"),
            new KeyValuePair<string, string>("DateTime","Data"),

        };

        public static BaseField PropertyToNeoField(PropertyInfo prop)
        {
            bool isMultiValued = prop.PropertyType.IsArray;
            string propName = prop.Name.ToLower();
            return null;
        }

        public static BaseGroup ClassToNeoGroup(PropertyInfo prop)
        {
            bool isMultiValued = prop.PropertyType.IsArray;
            var type = prop.PropertyType;
            MethodInfo method = typeof(LbNeoMap).GetMethod("ClassToNeoBase");
            string propName = prop.Name.ToLower();
            MethodInfo generic = method.MakeGenericMethod(type);
            BaseContent content = generic.Invoke(null, null) as BaseContent;
            return null;
        }

        public static BaseMetadata CreateNeoMetadata(string neoBaseName)
        {
            return null;
        }

        public static BaseContent ClassToNeoBase<T>()
        {
            PropertyInfo[] properties = typeof(T).GetProperties();
            var baseName = typeof(T).Name.ToLower();
            bool isGroup = false;
            foreach (var item in properties)
            {

                isGroup = !item.PropertyType.IsPrimitive 
                    && !item.PropertyType.Name.Equals("String") && !item.PropertyType.Name.Equals("DateTime");

                if (item.PropertyType.IsArray)
                {
                    var eleType = item.PropertyType.GetElementType();
                    isGroup = !eleType.IsPrimitive
                        && !eleType.Name.Equals("String") && !eleType.Name.Equals("DateTime");
                }

                if (isGroup)
                {
                    ClassToNeoGroup(item);
                }
                else
                {
                    PropertyToNeoField(item);
                }
            }
            return null;
        }
    }
}
