﻿using System;
using System.Threading.Tasks;

namespace LightBaseNeoLib.Interfaces
{
    public interface ILightBaseNeoConfig
    {
        Uri LightBaseUri { get; set; }
    }
}
