﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LightBaseNeoLib.Interfaces
{
    public interface ILightbaseNeoClient : IDisposable
    {
        Task<HttpResponseMessage> Get(string path);
        Task<HttpResponseMessage> Put(string path, HttpContent httpContent);
        Task<HttpResponseMessage> Delete(string path);
        Task<HttpResponseMessage> Patch(string path, List<KeyValuePair<string, string>> data);
        Task<HttpResponseMessage> Post(string path, List<KeyValuePair<string, string>> data );
        Task<HttpResponseMessage> PostFile(string path, StreamContent stream, string name, string fileName);
        Task<HttpResponseMessage> PostRawJson(string path, string rawData);
    }
}
