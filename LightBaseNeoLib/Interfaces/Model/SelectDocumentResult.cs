﻿using System;
using System.Collections.Generic;
using System.Json;
using System.Text;

namespace LightBaseNeoLib.Interfaces.Model
{
    public class SelectDocumentResult<T>
    {
        public int Limit { get; set; }
        public int Offset { get; set; }
        public int Result_Count { get; set; }
        public IList<T> Results { get; set; }
    }
}
