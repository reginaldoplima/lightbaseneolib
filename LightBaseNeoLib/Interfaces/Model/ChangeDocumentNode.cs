﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightBaseNeoLib.Interfaces.Model
{
    /// <summary>
    /// http://www.brlight.org/en/documentacao/#alterar-parcialmente-no-de-documentos-patch
    /// </summary>
    public class ChangeDocumentNode
    {
        public string Path { get; set; }
        public string Mode { get; set; }
        public string Fn { get; set; }
        public string[] Args { get; set; }
    }
}
