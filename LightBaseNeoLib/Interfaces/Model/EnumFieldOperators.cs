﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightBaseNeoLib.Interfaces.Model
{
    public sealed class EnumFieldOperators
    {
        private EnumFieldOperators() { }

        public const string Equal = "=";
        public const string EqualOrBigger = "=>";
        public const string EqualOrLess = "=<";
        public const string Bigger = ">";
        public const string Less = "<";
        public const string Contains = "contains";
        public const string Like = "like";
    }
}
