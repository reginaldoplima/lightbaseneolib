﻿using System.Runtime.Serialization;

namespace LightBaseNeoLib.Interfaces
{
    public class GroupMetadata
    {
        public string Alias { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public bool Multivalued { get; set; }
    }
}
