﻿using System.Collections.Generic;

namespace LightBaseNeoLib.Interfaces.Model
{
    public class SelectBaseResult
    {
        public int Row_Count { get; set; }
        public List<BaseResult>  Rows { get; set; }
    }

    public class BaseResult
    {
        public int Id_Base { get; set; }
        public string Name { get; set; }
    }
}
