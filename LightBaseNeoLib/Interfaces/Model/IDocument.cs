﻿namespace LightBaseNeoLib.Interfaces.Model
{
    public class IDocument
    {
        public IDocument(DocumentMetadata metadata)
        {
            _metadata = metadata;
        }
        public DocumentMetadata _metadata { get; set; }

    }

    public class DocumentMetadata
    {
        public int Id_Doc { get; set; }
        public string Dt_Last_Up { get; set; }
    }
}
