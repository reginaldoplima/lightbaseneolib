﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightBaseNeoLib.Interfaces.Model
{
    public class SelectFilters
    {
        public string Field { get; set; }
        public string Term { get; set; }
        public string Operetion { get; set; }
    }

    public class SelectWithFilters
    {
        public string[] Select { get; set; }
        public List<SelectFilters> Filters { get; set; }
    }
}
