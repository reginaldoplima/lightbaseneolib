﻿using LightBaseNeoLib.Utils;
using System.Runtime.Serialization;

namespace LightBaseNeoLib.Interfaces
{
    public class BaseField
    {

        public string Name { get; set; }
        public string Description { get; set; }
        public string Datatype { get; set; }
        public bool Required { get; set; }
        public string Alias { get; set; }
        public bool Multivalued { get; set; }
        public string[] Indices { get; set; }
    }
}
