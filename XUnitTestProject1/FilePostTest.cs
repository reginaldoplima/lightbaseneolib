﻿using LightBaseNeoLib.Core;
using LightBaseNeoLib.Interfaces;
using LightBaseNeoLib.Interfaces.Model;
using LightBaseNeoLib.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Runtime.Serialization;
using Xunit;

namespace XUnitTestProject1
{
    public class FilePostTest
    {
        private static ISerializer _ser = new LightBaseNeoLib.Utils.JsonSerializer();
        private static Uri _lbneoUri = new Uri("http://localhost:6543");
        private static ILightBaseNeoConfig _conf = new ClientConfig(_lbneoUri);
        private static ILightbaseNeoClient _lbneoclient = new LightBaseNeoClient(_conf);
        private static ILightBaseNeoApp _clientApp = new LightBaseNeoApp(_lbneoclient, _ser);

        //[Fact]
        public void Can_send_file()
        {
            StreamReader r = new StreamReader("../../../../LightBaseNeoLib/Scritps/app_user.json");
            StreamContent streamContent = new StreamContent(r.BaseStream);

            var result = _lbneoclient.PostFile("app_user/file",streamContent,"file","fdfds").Result;
        }

        [Fact]
        public void Can_send_file_to_base()
        {
            StreamReader r = new StreamReader("../../../../LightBaseNeoLib/Scritps/foto.jpg");
            StreamContent streamContent = new StreamContent(r.BaseStream);

            var result = _clientApp.InsertFileIntoBase("app_user", streamContent, "fdfds").Result;
        }
        // [Fact]
        public void Can_insert_file()
        {
            FileStream fs = new FileStream("../../../../LightBaseNeoLib/Scritps/app_user.json", FileMode.Open);
            var result = _clientApp.InsertFile(fs, "insert_file_t2","3","arq1").Result;
        }

        [Fact]
        public void Can_delet_document()
        {
            var result = _clientApp.DeleteDocument("3", "insert_file_t2").Result;
        }

    }
}
