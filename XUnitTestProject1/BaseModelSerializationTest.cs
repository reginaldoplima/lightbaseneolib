using LightBaseNeoLib.Core;
using LightBaseNeoLib.Interfaces;
using LightBaseNeoLib.Interfaces.Model;
using LightBaseNeoLib.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using Xunit;

namespace XUnitTestProject1
{
    [TestCaseOrderer("XUnitTestProject1.TestCollectionOrderer", "TestCollectionOrderer")]
    public class BaseModelSerializationTest
    {
        private static ISerializer _ser = new LightBaseNeoLib.Utils.JsonSerializer();
        private static Uri _lbneoUri = new Uri("http://localhost:6543");
        private static ILightBaseNeoConfig _conf = new ClientConfig(_lbneoUri);
        private static ILightbaseNeoClient _lbneoclient = new LightBaseNeoClient(_conf);
        private static ILightBaseNeoApp _clientApp = new LightBaseNeoApp(_lbneoclient, _ser);
        private static string baseName = "";
        private static int doc_id = -1;

        
        //[Fact]
        public void Can_Create_Base_By_Model()
        {
            var content = new BaseContent { };
            var metada = new BaseMetadata
            {
                Color = "#000",
                Name = "name01",
                Password = "sasasa",
                Description = "description",
                Dt_Base = new DateTime(1511353676492).ToString(),

            };
            var fileds = new List<BaseField>
            {
                new BaseField
                {
                    Alias = "alias1",
                    Description = "desc",
                    Multivalued = false,
                    Name = "name01",
                    Datatype = "Text",
                    Required = false,
                    Indices = new string[] { }
                },
                new BaseField
                {
                    Alias = "alias2",
                    Description = "desc2",
                    Multivalued = false,
                    Name = "name021",
                    Datatype = "Text",
                    Required = false,
                    Indices = new string[] { }
                },
            };

            //var groups = new List<BaseGroup>
            //{
            //    new BaseGroup
            //    {

            //        Content = new BaseContent
            //        {
            //            Fields = new List<BaseField>
            //            {
            //                                new BaseField
            //    {
            //        Alias = "alias1",
            //        Description = "desc",
            //        Multivalued = false,
            //        Name = "name01",
            //        Datatype = "Text",
            //        Required = false,
            //        Indices = new string[] { }
            //    },
            //    new BaseField
            //    {
            //        Alias = "alias2",
            //        Description = "desc2",
            //        Multivalued = false,
            //        Name = "name021",
            //        Datatype = "Text",
            //        Required = false,
            //        Indices = new string[] { }
            //    },
            //            }
            //        },

            //        Metadata = new GroupMetadata
            //        {
            //            Alias = "fdsfs",
            //            Description = "dsa",
            //            Multivalued = false,
            //            Name = "dsadas"
            //        }
            //    }

            //};

            //content.Fields = fileds;
            content.Metadata = metada;
            //content.Groups = groups;
            var str = content.ToString();
            string valid = "{ \"content\":[{ \"field\":{\"alias\":\"alias1\",\"datatype\":\"Text\",\"description\":\"desc\",\"indices\":[],\"multivalued\":false,\"name\":\"name01\",\"required\":false}},{ \"field\":{\"alias\":\"alias2\",\"datatype\":\"Text\",\"description\":\"desc2\",\"indices\":[],\"multivalued\":false,"
                + "\"name\":\"name021\",\"required\":false}},{ \"group\":{ \"content\":[{ \"field\":{\"alias\":\"alias1\",\"datatype\":\"Text\",\"description\":\"desc\",\"indices\":[],\"multivalued\":false,\"name\":\"name01\",\"required\":false}},{ \"field\":{\"alias\":\"alias2\",\"datatype\":\"Text\",\"description\":\"desc2\",\"indices\":[],\"multivalued\":false,\"name\":\"name021\",\"required\":false}}],\"metadata\":{\"alias\":\"fdsfs\",\"description\":\"dsa\",\"multivalued\":false,\"name\":\"dsadas\"}}}]," +
                "\"metadata\":{\"admin_users\":[\"\"],\"color\":\"#000\",\"description\":\"description\",\"dt_base\":\"/Date(1511353676492-0300)/\",\"file_ext\":false,\"file_ext_time\":0,\"id_base\":-1,\"idx_exp\":false,\"idx_exp_time\":0,\"idx_exp_url\":\"\",\"model\":null,\"name\":\"name01\",\"owner\":\"\",\"password\":\"sasasa\"}}\"";
            Assert.Equal(str, str);
        }

        [Fact, TestPriorityAttribute(-11)]
        public void Can_Zet_document_ByID()
        {
            var result = _clientApp.GetDocument<DocumentInsert>(doc_id.ToString(), baseName).Result;
            Assert.Equal(result._metadata.Id_Doc, doc_id);
        }


        [Fact, TestPriorityAttribute(7)]
        public void Can_Delete()
        {
            var result = _clientApp.DeleteBase(baseName).Result;
            Assert.Equal(result.StatusCode.ToString(), "OK");
        }

        [Fact, TestPriorityAttribute(-1)]
        public void Can_create_Base_By_NeoBase()
        {
            var neoBase = new NeoBase
            {
                Content = new List<BaseContentItem>
                {
                   new BaseContentItem
                   {
                       Field = new BaseField
                       {
                           Alias = "alias",
                           Datatype = "Text",
                           Description = "Description",
                           Indices = new string[]{ },
                           Multivalued = false,
                           Name = "email_user",
                           Required = false
                       }
                   },
                   new BaseContentItem
                   {
                       Field = new BaseField
                       {
                           Alias = "alias",
                           Datatype = "Text",
                           Description = "Description",
                           Indices = new string[]{ },
                           Multivalued = false,
                           Name = "id_user",
                           Required = false
                       }
                   },
                   new BaseContentItem
                   {
                       Field = new BaseField
                       {
                           Alias = "alias",
                           Datatype = "Text",
                           Description = "Description",
                           Indices = new string[]{ },
                           Multivalued = false,
                           Name = "name",
                           Required = false
                       }
                   },
                   new BaseContentItem
                   {
                       Field = new BaseField
                       {
                           Alias = "alias",
                           Datatype = "Text",
                           Description = "Description",
                           Indices = new string[]{ },
                           Multivalued = false,
                           Name = "passwd_user",
                           Required = false
                       }
                   },
                   new BaseContentItem
                   {
                       Group = new BaseGroup
                       {
                           Content = new List<BaseContentItem>
                           {
                               new BaseContentItem
                               {
                                   Field  = new BaseField
                                   {
                                       Alias  = "grupitem",
                                       Required = false,
                                       Datatype = "Date",
                                       Description = "grupo item",
                                       Indices = new string[]{ },
                                       Multivalued = true,
                                       Name = "groupitem"
                                   }
                               }
                           },
                           Metadata = new GroupMetadata
                           {
                               Alias = "GroupMetadata",
                               Description = "GroupMetadata",
                               Name = "grupometa",
                               Multivalued = false
                           }
                       }
                   }
                },
            };
            neoBase.Metadata.Name = "teste_xunit";
            var resonse = _clientApp.CreateBase(neoBase).Result;
            baseName = neoBase.Metadata.Name;
            Assert.True(resonse.IsSuccessStatusCode);
        }

        [Fact, TestPriorityAttribute(10)]
        public void Can_select_bases()
        {
            var result = _clientApp.SelectBases(baseName).Result;
            var existBase = result.Rows.Exists(bres => baseName.Equals(bres.Name));
            Assert.True(existBase);
        }

        [Fact, TestPriorityAttribute(0)]
        public void Can_create_doc()
        {
            DocumentInsert newDocument = new DocumentInsert(new DocumentMetadata())
            {
                email_user = "email@emaiwl.com",
                id_user = "wqwqwqw",
                name = "wqwqw",
                passwd_user = "wqwq",
                group_datas = new string[] {"21/11/17","11/09/01" }
            };
            var result = _clientApp.InsertDocument(newDocument, baseName).Result;
            doc_id = result;
            Assert.True(doc_id >= 1);

        }



        //[Fact]
        public void can_create_neobase_from_class()
        {
            var neoBase = new NeoBase
            {
                Content = new List<BaseContentItem>
                {   

                }
            };

            var teste = LbNeoMap.ClassToNeoBase<TesteCreate>();

        }



        //[Fact]
        public void selectFilter_to_sjon()
        {
            string valid = @"{ 'select':['id_reg','json_reg'],'filters':[{'field':'dt_index_tex','term':null,'operation':'='}]}";
            SelectWithFilters selectObj = new SelectWithFilters
            {
                Select = new string[] { "id_reg", "json_reg" },
                Filters = new List<SelectFilters>
                {
                    new SelectFilters
                    {
                        Field = "dt_index_tex",
                        Operetion = EnumFieldOperators.Equal,
                        Term = null
                    }
                }
            };
            var json = _ser.Serialize<SelectWithFilters>(selectObj);

        }





        //[Fact]
        public void teste_json_serializer()
        {
            var obj = LoadJson();
            var v = obj.Content;
        }

        public BaseContent LoadJson()
        {
            BaseContent item;
            using (StreamReader r = new StreamReader("../../../../LightBaseNeoLib/Scritps/app_user.json"))
            {
                string json = r.ReadToEnd();
                item = JsonConvert.DeserializeObject<BaseContent>(json);
            }
            return item;
        }

        //private List<BaseGroup> _getGroups()
        //{
        //    var groups = new List<BaseGroup>
        //    {
        //        new BaseGroup
        //        {
        //            Metadata = new GroupMetadata
        //            {
        //                Alias = "grupos",
        //                Description = "grupos",
        //                Multivalued = false,
        //                Name = "grupos"
        //            }
        //        }
        //    };
        //    return null;
        //}
        private List<BaseField> _getFields()
        {

            var fileds = new List<BaseField>
            {
                new BaseField
                {
                    Alias = "inteiros",
                    Description = "inteiros",
                    Multivalued = false,
                    //Name = "inteiros",
                    Datatype = "Text",
                    Required = false,
                    Indices = new string[] { }
                },
                new BaseField
                {
                    Alias = "Texts",
                    Description = "desc2",
                    Multivalued = false,
                    //Name = "Texts",
                    Datatype = "Text",
                    Required = false,
                    Indices = new string[] { }
                },
                new BaseField
                {
                    Alias = "colecao",
                    Description = "desc2",
                    Multivalued = true,
                    //Name = "colecao",
                    Datatype = "Text",
                    Required = false,
                    Indices = new string[] { }
                },

                new BaseField
                {
                    Alias = "Data",
                    Description = "desc2",
                    Multivalued = false,
                    //Name = "Data",
                    Datatype = "Text",
                    Required = false,
                    Indices = new string[] { }
                },
            };
            return fileds;
        }
    }



    public class DocumentInsert : IDocument
    {
        public DocumentInsert(DocumentMetadata metadata) : base(metadata)
        {
        }

        public string id_user { get; set; }
        public string name { get; set; }
        public string email_user { get; set; }
        public string passwd_user { get; set; }

        public string[] group_datas { get; set; }

    }

    public class TesteCreate
    {
        public int inteiros { get; set; }
        public string Texts { get; set; }
        public int[] colecao { get; set; }
        public DateTime Data { get; set; }
        public GrupoTeste Grupos { get; set; }
        public GrupoTeste[] colecaoGruops { get; set; }

    }

    public class Field
    {
        public string name { get; set; }
    };

    public class AnonObj
    {
        public Field field;
        public GroupItem group { get; set; }
    }

    public class BItem
    {

    }

    public class GroupItem
    {
        public IList<AnonObj> content { get; set; }
        public Metadata metadata { get; set; }
    }

    public class NBase
    {
        public IList<AnonObj> content { get; set; }
        public Metadata metadata { get; set; }
    }

    public class Metadata
    {
        public string name { get; set; }
        public bool multivalued { get; set; }
    };

    public class GrupoTeste
    {
        public int inteiros { get; set; }
        public string Texts { get; set; }
        public int[] colecao { get; set; }
        public DateTime Data { get; set; }
    }
}
