﻿using System;

public class TestPriorityAttribute : Attribute
{
    public int Priority { get; set; }
    public TestPriorityAttribute(int Priority)
    {
        this.Priority = Priority;
    }
}