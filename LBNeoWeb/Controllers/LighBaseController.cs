﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using LightBaseNeoLib.Interfaces;
using LightBaseNeoLib.Interfaces.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LBNeoWeb.Controllers
{
    [Produces("application/json")]
    [Route("api/lbg")]
    public class LighBaseController : Controller
    {
        private readonly ILightBaseNeoApp _lightBaseNeoApp;

        public LighBaseController(ILightBaseNeoApp lightBaseNeoApp)
        {
            _lightBaseNeoApp = lightBaseNeoApp;
        }

        [HttpGet("bases")]

        public async Task<SelectBaseResult> GetBases()

        {
            var result = await _lightBaseNeoApp.SelectBases("");
            return result;
        }
    }
}