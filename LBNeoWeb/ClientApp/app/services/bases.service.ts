import { SelectBaseResult } from './../model/select-base-result.model';
import { Http } from '@angular/http';
import { Injectable } from "@angular/core";

@Injectable()
export class BasesService {

    constructor(http: Http)  {
        
    }

    getBases () {
        this.http.get(baseUrl + 'api/lbg/bases').subscribe(result => {
            result.json() as SelectBaseResult[];
        }, error => console.error(error));
    }
}